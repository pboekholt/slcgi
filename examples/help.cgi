#!/usr/bin/env slsh
require("cgilib", "CGI");
require("slshhelp");

private define markup_doc(str)
{
   variable newstr = ""B;
   variable pos = 0, newpos, len;
   while(string_match(str, "`[^']*'", pos+1))
     {
	(newpos, len) = string_match_nth(0);
	newstr += str[[pos:newpos-1]];
	newstr += sprintf("<i>%s</i>", str[[newpos:newpos+len-1]]);
	pos = newpos + len;
     }
   newstr += str[[pos:]];
   return newstr;
}

private define help()
{
   variable subj = CGI->param("subj");
   variable doc, matches;
   ()=fputs(`<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 FINAL//EN">
<html>
<head>
<title>S-Lang help</title>
</head>
<body>
`, stdout);
   if(subj != NULL)
     {
	doc = slsh_get_doc_string(subj);
	if (doc == NULL)
	  {
	     matches = _apropos("Global", subj, 0xF);
	     ()=printf("no documentation for %s<br>", CGI->escapeHTML(subj));
	     if (matches != NULL && length(matches))
	       {
		  ()=fputs("Matching subjects:<br>\n", stdout);
		  variable match;
		  foreach match (matches)
		    ()=printf("<a href=\"help.cgi?subj=%s\">%s</a><br>\n", match, match);
	       }
	  }
	else
	  {
	     variable pos = is_substr(doc, "SEE ALSO\n");
	     if (pos)
	       {
		  variable rest, word;
		  rest = substr(doc, pos+10, -1);
		  doc=markup_doc(CGI->escapeHTML(substr(doc, 1, pos+10)));
		  foreach word (strtok(CGI->escapeHTML(rest), ", \n"))
		    doc += " <a href=\"help.cgi?subj=$word\">$word</a>"$;
	       }
	     else
	       {
		  doc = markup_doc(CGI->escapeHTML(doc));
	       }
	     ()=printf("<pre>%s</pre>",doc);
	  }
     }
   ()=fputs(`<form method="POST" action="help.cgi">
<br>Enter a SLang function or variable <input type="text" name="subj" value="">
<input type="submit">
</form>
</body>
</html>
`, stdout);
}

CGI->init();
CGI->header();
help();
