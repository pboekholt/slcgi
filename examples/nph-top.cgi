#!/usr/bin/env slsh
% server-push interface to top
require("cgilib", "CGI");
variable boundary = "------- =_aaaaaaaaaa0";
variable separator = "\n--$boundary\n"$;
CGI->init();
CGI->header(;nph);
()=fputs(`Content-Type: multipart/x-mixed-replace;boundary="$boundary"

WARNING: YOUR BROWSER DOESN'T SUPPORT THIS SERVER-PUSH TECHNOLOGY.$separator`$, stdout);

variable top = popen("top -b -n 100", "r");
if (top == NULL) throw RunTimeError, "could not start top";
variable line;
variable output=""B;
foreach line(top)
{
   if (bstrlen(output) && not strncmp(line, "top", 3))
     {
	()=fputs("Content-Type: text/plain\n\n", stdout);
	()=fputs("nph-top (running)\n", stdout);
	()=fputs(output, stdout);
	()=printf("\n--%s\n", boundary);
	if (0 != fflush(stdout))
	  throw RunTimeError, "could not flush stdout: $errno"$;
	output = ""B;
     }
   output +=line;
}
sleep(3);
()=fputs("Content-Type: text/plain\n\n", stdout);
()=fputs("nph-top (finished)\n", stdout);
()=fputs(output, stdout);
()=printf("\n--%s--\n", boundary);
()=fputs("WARNING: YOUR BROWSER DOESN'T SUPPORT THIS SERVER-PUSH TECHNOLOGY.\n", stdout);
()=fclose(top);
