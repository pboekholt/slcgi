#!/usr/bin/env slsh
% script for plotting parametric equations
require ("png");
require("onig");

require("cgilib", "CGI");

define make_tmp_file ()
{
   variable pid = getpid (), i, file;
   _for i  (0, 1000, 1)
     {
	file = sprintf ("/tmp/%d.%d", pid, i);
	if (stat_file(file) == NULL && errno==ENOENT) return file;
     }
   throw RunTimeError, "Unable to create a tmp file!";
}

define xfunc(t)
{
   t;
}

define yfunc(t)
{
   t;
}

define slsh_main()
{
   CGI->init();
   variable xmin, ymin, xmax, ymax, tmin, tmax;
   xmin = CGI->param("xmin"; type="int");
   xmax = CGI->param("xmax"; type="int");
   ymin = CGI->param("ymin"; type="int");
   ymax = CGI->param("ymax"; type="int");
   tmin = CGI->param("tmin"; type="int");
   tmax = CGI->param("tmax"; type="int");
   variable x=CGI->param("xfunc");
   variable y=CGI->param("yfunc");
   try
     {
	% You shouldn't put this on the Internet. But just in case, we won't
	% evaluate arbitrary code.
	variable re = onig_new(`^(
[-\d\.+*/^() ]                                   # non-word characters
|\b(PI|abs|sqrt|exp|log|a?(sin|cos|tan)h?|t)\b   # words
){1,50}$ `, ONIG_OPTION_EXTEND);
	ifnot (onig_search(re, x) && onig_search(re, y))
	  throw SyntaxError;
	eval("define xfunc(t) { $x ; }"$);
	eval("define yfunc(t) { $y ; }"$);
     }
   catch SyntaxError:
     {
	x = "sin(2*t)";
	y = "sin(t)/exp(sin(5*t))";
	(xmin, xmax, ymin, ymax, tmin, tmax) = (-3, 3, -3, 3, 0, 10);
	eval("define xfunc(t) { $x ; }"$);
	eval("define yfunc(t) { $y ; }"$);
     }
   variable ydim = (ymax-ymin) * 100;
   variable xdim = (xmax - xmin) * 100;
   variable image = @UChar_Type[ydim, xdim];
   image[*]=10;
   if ( ymin < 0 < ymax)
     {
	image[-ymin * 100, *] = 100;
     }
   if (xmin < 0 < ymax)
     {
	image[*, -xmin * 100] = 100;
     }
   
   variable t;
   for (t=0.0 + tmin; t < tmax; t += 0.002)
     {
	x = xfunc(t);
	y = yfunc(t);
	x = int((x - xmin) * 100);
	y = int((y - ymin) * 100);
	if (0 < y < ydim && 0 < x < xdim)
	  image[y,x]=200;
     }
   variable file = make_tmp_file();
   png_write_flipped(file, image);
   CGI->header("image/png");
   variable fp = fopen(file, "r");
   variable data;
   while(-1 != fread_bytes(&data, 4096, fp))
     ()=fwrite(data, stdout);
   ()=fclose(fp);
   ()=remove(file);
}
