\function{init}
\synopsis{initialize a CGI script}
\usage{init()}
\description
  The \exmp{init} function initializes the variables used by \sfun{param},
  \sfun{params}, \sfun{get_cookies} and \sfun{cookie}.
\seealso{param, params, get_cookies, cookie}
\done

\function{request_method}
\synopsis{return the request method of the CGI script}
\usage{request_method()}
\description
  Return the request method of the CGI script.
\seealso{init, param, params, get_cookies, cookie, set_cookie, header}
\done

\function{param}
\synopsis{return a single CGI parameter}
\usage{param(String_Type key [;qualifiers])}
\description
  This returns a single CGI parameter named \exmp{key}.
  Supported qualifiers:
#v+
  type=[string|str|int|float|double]
#v-
   converts it to the desired type.
#v+
  default
#v-
   Sets a default value
\seealso{init, request_method, params, get_cookies, cookie}
\done

\function{params}
\synopsis{return a multiple CGI parameter}
\usage{params(String_Type key [;type=[string|str|int|float|double]])}
\description
  This returns all the CGI parameters named \exmp{key} as an array. If this
  parameter does not exist, NULL or an empty array of the requested type is
  returned.
\seealso{init, request_method, param, get_cookies, cookie}
\done

\function{get_cookies}
\synopsis{get the request cookies}
\usage{get_cookies()}
\description
  Return the names of the request cookies.
\seealso{init, request_method, param, params, cookie, set_cookie}
\done

\function{cookie}
\synopsis{return the value of a request cookie}
\usage{cookie(String_Type key [;qualifiers])}
\description
  Returns the value of the cookie named \exmp{key}.
  Supported qualifiers:
#v+
  type=[string|str|int|float|double]
#v-
   converts it to the desired type.
#v+
  default
#v-
   Sets a default value
\seealso{init, request_method, param, params, get_cookies, set_cookie}
\done

\function{set_cookie}
\synopsis{set a cookie}
\usage{set_cookie(String_Type name, String_Type value [;qualifiers])}
\description
  Set a cookie.
  The following qualifiers are supported:
#v+
  domain
#v-
   Sets the cookie domain
#v+
  path
#v-
   Sets the cookie path
#v+
  expires=[string|int]
#v-
   Sets the expiration date of the cookie. This can be a timestamp, or it can
   be a string of the form
#v+
          +30s                              30 seconds from now
          +10m                              ten minutes from now
          +1h                               one hour from now
          -1d                               yesterday (i.e. "ASAP!")
          now                               immediately
          +3M                               in three months
          +10y                              in ten years time
          Thursday, 25-Apr-1999 00:40:33 GMT  at the indicated time & date
#v-
\seealso{init, get_cookies, cookie, header}
\done

\function{header}
\synopsis{}
\usage{header()}
\description
  Send the CGI header.
  The following qualifiers are supported:
#v+
  redirect=url
#v-
   generates a location redirect to \exmp{url}
#v+
  status=int  
#v-
   sets the response status.
#v+
  nph
#v-
   If this is set, \exmp{header} will issue the correct headers to work with a NPH (no-parse-header)
   script.
#v+
  charset
#v-
   This can be used to control the character set sent to the browser.  If not provided, defaults to
   UTF-8 when in utf8 mode, ISO-8859-1 otherwise.
  
  Other name=value qualifiers are sent as additional headers.
\seealso{init, request_method, set_cookie}
\done

\function{escapeHTML}
\synopsis{escape special characters in HTML}
\usage{String_Type escapeHTML(String_Type str)}
\description
  The \exmp{escapeHTML} function repaces the special charaters ``&'', ``<'',
  ``>'' and ``"'' by html entities.
\done
