% cgilib.sl
% 
% Copyright (c) 2009, 2010 Paul Boekholt.
% Released under the terms of the GNU GPL (version 2 or later).
% 
% A simple CGI library. Requires S-Lang 2.2
% 
% Version: 0.1.0

typedef struct
{
   filename, content_type, data
} Upload_Type;

new_exception("CGIError", RunTimeError, "CGI error");

private variable parameters = Assoc_Type[List_Type];
private variable cookies = Assoc_Type[String_Type];
private variable response_cookies = {};

private define cgi_unescape(s)
{
   variable len = strbytelen(s), pos=1, escape;
   (s,) = strreplace(s, "%20", "+", len);
   forever 
     {
	pos = string_match(s, "%[0-9A-Fa-f][0-9A-Fa-f]", pos);
	ifnot(pos) break;
	escape = substrbytes(s, pos, 3);
	if (escape == "%00" || escape == "%25" || escape == "%2B" || escape == "%2b")
	  {
	     pos++;
	     continue;
	  }
	(s,) = strreplace(s, escape, char(integer("-0x" + escape[[1:]])), len);
     }
   if (is_substr(s, "%00"))
     {
	variable s2;
	(s2,) = strreplace(s,  "%00", " ", len);
	(s2,) = strreplace(s2, "%2B", "+", len);
	(s2,) = strreplace(s2, "%2b", "+", len);
	(s2,) = strreplace(s2, "%25", "%", len);
	s2 = bstring_to_array(s2);
	(s,) = strreplace(s, "+",   " ", len);
	(s,) = strreplace(s, "%2B", "+", len);
	(s,) = strreplace(s, "%2b", "+", len);
	(s,) = strreplace(s, "%25", "%", len);
	s=bstring_to_array(s);
	s[[where(s2==' ')]] = 0;
	return array_to_bstring(s);
     }
   else
     {
	(s,) = strreplace(s, "+",   " ", len);
	(s,) = strreplace(s, "%2B", "+", len);
	(s,) = strreplace(s, "%2b", "+", len);
	(s,) = strreplace(s, "%25", "%", len);
	return s;
     }
}

% This is adapted from the 'translate' script in the curl module
private define make_encode_table ()
{
   variable table = array_map (String_Type, &sprintf, ("%%%02X", [0:255]));
   variable ok = [['A':'Z'], ['a':'z'], ['0':'9'], '.', '-', '*', '_', '/', '~'];
   table[ok] = array_map (String_Type, &char, ok);
   table[' '] = "+";
   return table;
}

private variable encode_table = NULL;

private define escape (text)
{
   if (encode_table == NULL)
     encode_table = make_encode_table();
   variable len = bstrlen (text);
   variable new_text = String_Type[len];
   variable i;
   _for i (0, len-1, 1)
     new_text[i] =  encode_table[text[i]];
   return strjoin (new_text, "");
}

private define parse_params(input)
{
   variable param, pos, key;
   foreach param(strtok(input, "&;"))
     {
	pos = is_substr(param, "=");
	key = cgi_unescape(substr(param, 1, pos-1));
	ifnot(assoc_key_exists(parameters, key))
	  {
	     parameters[key] = {};
	  }
	list_append(parameters[key], cgi_unescape(substr(param, pos+1, -1)));
     }
}

private define parse_part();
private define read_post_data();
private define parse_cookies();

#if (getenv("HTTP_COOKIE") != NULL)
require("onig", "");

private define parse_cookies()
{
   variable raw_cookie = getenv("HTTP_COOKIE");
   if (raw_cookie == NULL) return;
   variable pos = 0;
   variable re = onig_new("([\\w\\d]+)\\s*=\\s*(\"[^\"]*\"|[^\",;]*)");
   while(onig_search(re, raw_cookie, pos, -1))
     {
	cookies[onig_nth_substr(re, raw_cookie, 1)] = cgi_unescape(strtrim(onig_nth_substr(re, raw_cookie, 2), "\""));
	pos = onig_nth_match(re, 0)[1];
     }
}
#endif

#if (getenv("REQUEST_METHOD") == "POST")
#if (not is_substr(getenv("CONTENT_TYPE"), "x-www-form-urlencoded"))
require("onig", "");
private variable partno=0;
private define parse_part(part)
{
   variable pos = string_match(part, "\r\n\r\n", 1);
   partno++;
   ifnot(pos) {
      if (partno == 1)
	return;
      throw CGIError, "failed to parse part";
   }
   
   variable header = part[[:pos - 1]];

   variable re = onig_new("(?i)Content-Disposition:.* name=\"?([^\";\s]*)\"?");
   
   if (onig_search(re, header))
     {
	variable key = onig_nth_substr(re, header, 1);
	ifnot(assoc_key_exists(parameters, key))
	  {
	     parameters[key] = {};
	  }
	variable  filename_re = onig_new("(?i)Content-Disposition:.* filename=\"?([^\";\\s]*)\"?");
	if (onig_search(filename_re, header))
	  {
	     variable upload = @Upload_Type;
	     upload.filename = onig_nth_substr(filename_re, header, 1);
	     variable content_type_re = onig_new("(?i)Content-Type: ([^\\s]*)");
	     if (onig_search(content_type_re, header))
	       upload.content_type = onig_nth_substr(content_type_re, header, 1);
	     upload.data = part[[pos + 3:]];
	     list_append(parameters[key], upload);
	     return;
	  }
	list_append(parameters[key], part[[pos + 3:]]);
     }
}
#endif
private define read_post_data(bytes)
{
   variable input = ""B;
#if (is_substr(getenv("CONTENT_TYPE"), "x-www-form-urlencoded"))
   if(is_substr(getenv("CONTENT_TYPE"), "x-www-form-urlencoded"))
     {
	if (bytes != fread_bytes(&input, bytes, stdin))
	  {
	     throw CGIError, "could not read POST data";
	  }
	parameters["POSTDATA"] = {input};
	parse_params(input);
	return;
     }
#else
   variable re = onig_new("multipart/form-data.*boundary=\"?([^\";,]+)\"?");
   if (onig_search(re, getenv("CONTENT_TYPE")))
     { 
	variable boundary = "--"+onig_nth_substr(re, getenv("CONTENT_TYPE"), 1);
	variable boundarylen = strbytelen(boundary);
	variable o = onig_new(boundary, 0, "ascii", "asis");
	forever
	  {
	     if (bytes <= 0)
	       break;
	     variable s, bytes_read;
	     bytes_read = fread_bytes(&s, min([4096, bytes]), stdin);
	     if (bytes_read == -1)
	       {
		  throw CGIError, "could not read POST data";
	       }
	     bytes -= bytes_read;
	     input += s;
	     
	     variable pos, end;
	     pos = 0;
	     forever
	       {
		  end = onig_search(o, input, pos, -1);
		  ifnot(end)
		    {
		       if (pos)
			 input = input[[pos:]];
		       break;
		    }
		  end=onig_nth_match(o, 0)[0]+1;
		  parse_part(input[[pos:end -4]]); % \r\n at the end
		  pos=end+boundarylen;
	       }
	  }
     }
#endif
}
#endif

define init()
{
#if (getenv("QUERY_STRING") == NULL)
   ()=fputs("(offline mode: enter name=value pairs on standard input)\n", stderr);
   variable param, pos, key;
   foreach param(stdin)
     {
	pos = is_substr(param, "=");
	ifnot(pos) continue;
	key = substr(param, 1, pos-1);
	ifnot(assoc_key_exists(parameters, key))
	  {
	     parameters[key] = {};
	  }
	list_append(parameters[key], strtrim(substr(param, pos+1, -1)));
     }
#else
   parse_params(getenv("QUERY_STRING"));
   if (getenv("REQUEST_METHOD") == "POST")
     {
	variable bytes = atoi(getenv("CONTENT_LENGTH"));
	if (bytes > qualifier("post_max", 1048576))
	  {
	     throw CGIError, "POST data exceeds post_max";
	  }
	read_post_data(bytes);
     }
   parse_cookies();
#endif   
}

% request functions
% Type conversions are like in cmdopt.sl

private define convert(p, a, type, cb)
{
   switch (type)
     {
      case "string" or case "str":
	ifnot(assoc_key_exists(a, p))
	  return "";
	return @cb(a,p);
     }
     {
      case "int":
	ifnot(assoc_key_exists(a, p))
	  return 0;
	return atoi(@cb(a,p));
     }
     {
      case "float" or case "double":
	ifnot(assoc_key_exists(a, p))
	  return 0.0;
	return atof(@cb(a,p));
     }
     {
      case NULL:
	ifnot(assoc_key_exists(a, p))
	  return NULL;
	return @cb(a,p);
     }
     {
	throw InvalidParmError, sprintf ("type=%s is not supported", type);
     }
}

define request_method()
{
   return getenv("REQUEST_METHOD");
}

private define paramcallback(a, p)
{
   return a[p][0];
}

define param(key)
{ 
   if (qualifier_exists("default") && not assoc_key_exists(parameters, key))
     return qualifier("default");
  return convert(key, parameters, qualifier("type"), &paramcallback);
}

define params(key)
{
   switch(qualifier("type"))
     {
      case "string" or case "str":
	ifnot(assoc_key_exists(parameters, key))
	  return String_Type[0];
	return list_to_array(parameters[key]);
     }
     {
      case "int":
	ifnot(assoc_key_exists(parameters, key))
	  return Integer_Type[0];
	return atoi(list_to_array(parameters[key]));
     }
     {
      case "float" or case "double":
	ifnot(assoc_key_exists(parameters, key))
	  return Double_Type[0];
	return atof(list_to_array(parameters[key]));
     }
     {
	ifnot(assoc_key_exists(parameters, key))
	  return NULL;
	return list_to_array(parameters[key]);
     }
     {
	throw InvalidParmError, sprintf ("type=%s is not supported", qualifier("type"));
     }	
}

define get_cookies()
{
   return assoc_get_keys(cookies);
}

private define cookiecallback(a, p)
{
   return a[p];
}

define cookie(key)
{
   if (qualifier_exists("default") && not assoc_key_exists(cookies, key))
     return qualifier("default");
   return convert(key, cookies, qualifier("type"), &cookiecallback);
}

% response functions

private define  rfc1123_date()
{
   variable t = gmtime(_NARGS? () : _time());
   variable rfc822_days = "Sun Mon Tue Wed Thu Fri Sat";
   variable rfc822_months = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec";
   return sprintf("%s, %02d %s %04d %02d:%02d:%02d GMT",  extract_element(rfc822_days, t.tm_wday, ' '),
		  t.tm_mday, extract_element(rfc822_months, t.tm_mon, ' '),
		  1900+ t.tm_year, t.tm_hour, t.tm_min, t.tm_sec);
}

private define expires(t)
{
   switch(_typeof(t))
     {
      case Integer_Type:
	return rfc1123_date(t);
     }
     {
      case String_Type:
	if (t == "now") return rfc1123_date(_time());
	variable mult = struct {
	   s=1,
	   m=60,
	   h=3600,
	   d=86400,
	   M=2592000,
	   y=946080000
	};
	variable offset, m;
	if(sscanf(t, "%d%[smhdMY]", &offset, &m) && strlen(m))
	  return rfc1123_date(_time() + offset * get_struct_field(mult, m));
	return t;
     }
}

define set_cookie(name, value)
{
   variable param, s ="";
   if(__qualifiers() != NULL)
     {
	foreach param(["domain", "path"])
	  {
	     if (qualifier_exists(param))
	       {
		  s+=sprintf(";%s=%s", param, qualifier(param));
	       }
	  }
	if (qualifier_exists("expires"))
	  {
	     s+=sprintf(";expires=%s", expires(qualifier("expires")));
	  }
     }
   list_append(response_cookies, sprintf("set-cookie: %s=%s%s", name, escape(value),s));
}

define header()
{
   variable header;
   variable content_type = "text/html";
   if (_NARGS)
     content_type = ();
   if (qualifier_exists("redirect"))
     {
	()=printf("Location: %s\n", qualifier("redirect"));
     }
   else if (qualifier_exists ("nph"))
      {
	()=printf("%s %s\n", getenv("SERVER_PROTOCOL") != NULL ? getenv("SERVER_PROTOCOL") : "HTTP/1.0",
		  qualifier("status", "200 OK"));
	()=printf("Date: %s\n", rfc1123_date());
     }
   else
     {
	()=printf("Content-type: %s; charset=%s\n", content_type,
		  qualifier("charset", _slang_utf8_ok ? "UTF-8" : "ISO-8859-1"));
     }
   
   foreach header (response_cookies)
     {
	()=printf("%s\n", header);
     }
   variable options = __qualifiers();
   if (options != NULL)
     {
	variable option;
	foreach option (get_struct_field_names(options))
	  {
	     if (option == "redirect" || option == "nph" || option == "charset") continue;
	     ()=printf("%s: %S\n", strtrans(option, "_", "-"), (get_struct_field(options, option)));
	  }
     }
   ifnot(qualifier_exists("nph"))
     {
   	()=printf("\n");
     }
}

define escapeHTML(s)
{
   variable n = strlen(s);
   (s,)=strreplace(s, "&",  "&amp;",  n);
   (s,)=strreplace(s, "\"", "&quot;", n);
   (s,)=strreplace(s, ">",  "&gt;",   n);
   (s,)=strreplace(s, "<",  "&lt;",   n);
   return s;
}

provide("cgilib");
